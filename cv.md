Curriculum Vitæ
===============

Experiences
-----------

### Active

- September 2021 – March 2023: Association Régionale des Tsiganes et de leurs Amis Gadjé (ARTAG)

	- actions on halting sites around educational activites;
	- workshops around digital culture and practices;
	- mapping of services around heavy passage halting sites using OpenStreetMap;
	- assistance to CNED students;
	- assistance to the staff;

- September 2020 – now: Meriu Design (Spain)

    - website development of meriu.design

        - Project Management, HTML, CSS & JavaScript integration with Grav;
        - Development using Content-Driven approach;

- October 2019 – now: Zenith ETN (Europe)

    - website development of zenith-etn.com

        - HTML, CSS & JavaScript integration with Grav, with design from Headquarters;


- August 2017 – now: Spinal Sensory Signalling (Paris)

    - website development of wyartlab.org

        - HTML, CSS & JavaScript integration with Bolt, with design from Flagship (Shanghai);

- November 2011 – now: W3C

    - Proof-reader and translator “Personal names around the world”;

- 2001 – now: Internet oriented programming (HTML, CSS, JS, PHP);

### Past

- October 2018: Elson (Paris)

    - Prototypes based on Graphic Design provided by client, integrated in React by a third party.

- 2018: Wes Bos’ ES6 for Everyone (#5ae930de477d322f9967f2e3)

- 2015 – 2016: Juwai (Shanghai)

    - Frontend Developer on juwai.com

        - HTML, CSS & JavaScript integration for administrative part of the website;
        - Local referent for an international team (mainly China and Philippines);

- 2015-01-28 – 2015-03-03: W3DevCampus: Mobile Web 2: Programming Web Applications

- 2014-09-09 – 2015-07-10: Intensive Chinese Training Program at East China Normal University


- 2010 – 2014: The NetCircle (Shanghai)

    - Web Designer + Frontend Developer on kaufmich.com & gays.com

        - Mockups (Illustrator + Photoshop) or prototypes (HTML, CSS & JavaScript integration);
        - Organize design related work between different teams.

    - 2011-09-18 – 2011-09-19: Certified Scrum Master training
    - 2013-11-11 – 2013-12-08: W3DevCampus: JavaScript for Beginners
    - 2014-01-24 – 2014-02-24: W3DevCampus: Responsive Web Design
    - Scrum Master + Frontend Developer on a 3+ Millions users website: poppen.de

- 2004 – 2014: Pompage.net (France)

    - translator, editor & website administrator: pompage.net/about

- 2006 – 2010: Micro Application (Paris)

    - co‑author for “CSS, Le guide complet”
    - https://archives.arkhi.org/projects/css-guide-complet/

- 10 July 2010: Kill the FM remixed

- June 2010: China-Europa Forum (Paris)

    - website development: china-europa-forum.net

- 6 May 2010: flipmood

- 2006 – 2009: Paris-Web (Paris)

    - staff member: paris-web.fr

- 13 September 2009: [bodybag]

- April – December 2008: Naço architectures (Shanghai)

    - project manager
    - 1 April 2008: Ibis hotel — Bangalore
    - 21 May 2008: ACCOR Academy
    - 17 July 2008: naço architectures ShangHai’s website
    - 27 November 2008: Footbridge
    - 21 December 2008: SiChuan Reconstruction

- July – September 2007: Guilin Institute for Architectural Design & Research (Guilin)

    - project manager
    - 19 July 2007: Xīnzhài’s elementary school
    - 13 August 2007: collective housing and shopping mall — GuiGang

- May – June 2007: PACER Architects Ltd. (Shanghai)

    - project manager
    - June 2007: EXPO 2010 Shanghai: Open solicitation for Chinese pavilion

- 31 March 2007: Website of Pub Piccone

- 30 March 2007: Kill the FM

- June 2004 – December 2005: ABC architecture – Hervé Saintis, architect (Toulouse)

    - internship to architect collaborator;
    - 23 December 2005: LAAS elevations
    - 23 July 2005: Banks and ATM planning

- June 2005: DPLG architect (Diplômé par le Gouvernement: Qualified by the Government); Between reality and virtuality, a mixed gaming space

- 30 January 2005: Make yourself a home

- 19 November 2004: non-architecture

- School years 2000 – 2004: IT instructor

    - National architecture college of Toulouse

- 26 April 2004: The archiforum

- 6 August 2003: madasondage

- 15 September 2002: architect’s lamp

- 1994 – 2001: Diploma of Fundamental Studies in Architecture (DEFA)

- March 1999 – July 2000: Atelier d'Eux – Karinne Jarry, architect (Montluel)

- March – June 1997; school years 1997 – 2000: National architecture college of Lyon: IT instructor

- 1 March 1999: BMW competition, highway rest area

- 1 June 1994: Reused Carton Chair Design



